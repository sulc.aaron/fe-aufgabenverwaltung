import { Aufgabe } from "./Aufgabe";

export class SuccessResponse {
    private endpoint: Endpoint;
    private responseCode: number;
    private data?: Array<Aufgabe>;

    constructor(
        endpoint: Endpoint,
        responseCode: number,
        data?: Array<Aufgabe>,
    ) {
        this.endpoint = endpoint;
        this.responseCode = responseCode;
        this.data = data;
    }

    public getEndpoint(): Endpoint {
        return this.endpoint;
    }

    public getResponseCode(): number {
        return this.responseCode;
    }

    public getData(): Array<Aufgabe>|null {
        return this.data ?? null;
    }
}

export enum Endpoint{
    PostAufgabeErstellen,
    PostAufgabeEntfernen,
    PostAufgabeAnpassen,
    GetAlleAufgaben,
};

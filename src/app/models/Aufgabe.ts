export class Aufgabe {
    private id: number;
    private titel: string;
    private beschreibung: string;

    constructor(
        id: number,
        titel: string,
        beschreibung: string,
    ) {
        this.id = id;
        this.titel = titel;
        this.beschreibung = beschreibung;
    }

    public getId(): number {
        return this.id;
    }

    public getTitel(): string {
        return this.titel;
    }

    public getBeschreibung(): string {
        return this.beschreibung;
    }

    public setTitel(titel: string): void {
        this.titel = titel;
    }

    public setBeschreibung(beschreibung: string): void {
        this.beschreibung = beschreibung;
    }
}
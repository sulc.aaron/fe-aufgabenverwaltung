import { Endpoint } from "./SuccessResponse";

export class ErrorResponse {
    private endpoint: Endpoint;
    private responseCode: number;
    private errorMessage: string;

    constructor(
        endpoint: Endpoint,
        responseCode: number,
        errorMessage: string,
    ) {
        this.endpoint = endpoint;
        this.responseCode = responseCode;
        this.errorMessage = errorMessage;
    }

    public getEndpoint(): Endpoint {
        return this.endpoint;
    }

    public getResponseCode(): number {
        return this.responseCode;
    }

    public getErrorMessage(): string {
        return this.errorMessage;
    }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AufgabenlisteComponent } from './aufgabenliste/aufgabenliste.component';

const routes: Routes = [
  {
    path: '', component: AufgabenlisteComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Aufgabe } from '../models/Aufgabe';

@Component({
  selector: 'app-aufgabe',
  templateUrl: './aufgabe.component.html',
  styleUrls: ['./aufgabe.component.scss']
})
export class AufgabeComponent {
  @Input() aufgabe!: Aufgabe;
  @Output() openEditFormEmitter = new EventEmitter<Aufgabe>();
  @Output() deleteAufgabeEmitter = new EventEmitter<number>();

  constructor() { }

  editAufgabe(): void {
    this.openEditFormEmitter.emit(this.aufgabe);
  }

  deleteAufgabe(): void {
    this.deleteAufgabeEmitter.emit(this.aufgabe.getId());
  }
}

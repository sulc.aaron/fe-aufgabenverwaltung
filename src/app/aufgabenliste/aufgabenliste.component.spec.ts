import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AufgabenlisteComponent } from './aufgabenliste.component';

describe('AufgabenlisteComponent', () => {
  let component: AufgabenlisteComponent;
  let fixture: ComponentFixture<AufgabenlisteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AufgabenlisteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AufgabenlisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Aufgabe } from '../models/Aufgabe';
import { AufgabenWebService } from '../services/aufgaben-web.service';
import { Endpoint, SuccessResponse } from '../models/SuccessResponse';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ErrorResponse } from '../models/ErrorResponse';

@Component({
  selector: 'app-aufgabenliste',
  templateUrl: './aufgabenliste.component.html',
  styleUrls: ['./aufgabenliste.component.scss']
})
export class AufgabenlisteComponent implements OnInit {
  aufgabenliste: Aufgabe[] = [];
  aufgabenWebService: AufgabenWebService;

  addForm: FormGroup;
  editForm: FormGroup;
  state: State = State.Default;
  currentEditId?: number;
  errorMessage?: string;
  loading: boolean = false;

  constructor(
    aufgabenWebService: AufgabenWebService,
    formBuilder: FormBuilder,
    ) {
    this.aufgabenWebService = aufgabenWebService;
    this.addForm = formBuilder.group({
      titel: [
        '',
        [Validators.required, Validators.maxLength(50), Validators.minLength(1)]
      ],
      beschreibung: [
        '',
        [Validators.required, Validators.maxLength(255), Validators.minLength(1)]
      ]
    });
    this.editForm = formBuilder.group({
      titel: [
        '',
        [Validators.required, Validators.maxLength(50), Validators.minLength(1)]
      ],
      beschreibung: [
        '',
        [Validators.required, Validators.maxLength(255), Validators.minLength(1)]
      ]
    });
  }

  ngOnInit(): void {
    this.subscribeServices();
    
    this.getAlleAufgaben();
  }

  subscribeServices(): void {
    this.aufgabenWebService.successEmitter.subscribe((successResponse: SuccessResponse) => {
      switch (successResponse.getEndpoint()) {
        case Endpoint.GetAlleAufgaben:
          const aufgabenliste = successResponse.getData();

          if (200 === successResponse.getResponseCode() && null !== aufgabenliste) {
            this.aufgabenliste = aufgabenliste;
          }

          break;
        case Endpoint.PostAufgabeErstellen:
        case Endpoint.PostAufgabeAnpassen:
          case Endpoint.PostAufgabeEntfernen:
          this.state = State.Default;
          this.getAlleAufgaben();
          this.closeForms();
          this.currentEditId = undefined;

          break;
      }
      this.loading = false;
    });

    this.aufgabenWebService.errorEmitter.subscribe((errorResponse: ErrorResponse) => {
      switch (errorResponse.getEndpoint()) {
        case Endpoint.GetAlleAufgaben:
          alert('Es kann keine Verbindung mit dem Server hergestellt werden. Bitte überprüfe die Verbindung und lade diese Seite neu.');

          break;
        case Endpoint.PostAufgabeErstellen:
        case Endpoint.PostAufgabeAnpassen:
          this.errorMessage = errorResponse.getErrorMessage();

          break;
        case Endpoint.PostAufgabeEntfernen:
          alert('Ein Fehler ist beim Löschen der Aufgabe aufgetreten. Bitte versuche es erneut.');
          this.getAlleAufgaben();

          break;
      }
      this.loading = false;
    });
  }

  openAddAufgabeForm(): void {
    this.state = State.Add;
    this.addForm.reset();
  }

  openEditAufgabeForm(aufgabe: Aufgabe): void {
    this.state = State.Edit;
    this.editForm.reset();

    this.currentEditId = aufgabe.getId();
    this.editForm.setValue({
      titel: aufgabe.getTitel(), 
      beschreibung: aufgabe.getBeschreibung()
    });
  }

  closeForms(): void {
    this.state = State.Default;
    this.currentEditId = undefined;
  }

  getAlleAufgaben(): void {
    this.loading = true;
    this.errorMessage = undefined;

    this.aufgabenWebService.getAlleAufgaben();
  }

  addAufgabe(): void {
    if (this.addForm.valid) {
      this.loading = true;
      this.errorMessage = undefined;
      
      const titel = this.addForm.get('titel')?.value;
      const beschreibung = this.addForm.get('beschreibung')?.value;

      this.aufgabenWebService.addAufgabe(titel, beschreibung);
    }
    else {
      this.addForm.markAllAsTouched();
    }
  }

  editAufgabe(): void {
    if (this.editForm.valid && undefined !== this.currentEditId) {
      this.loading = true;
      this.errorMessage = undefined;
      
      const titel = this.editForm.get('titel')?.value;
      const beschreibung = this.editForm.get('beschreibung')?.value;

      this.aufgabenWebService.editAufgabe(this.currentEditId, titel, beschreibung);
    }
    else {
      this.editForm.markAllAsTouched();
    }
  }

  deleteAufgabe(id: number): void {
    this.aufgabenWebService.deleteAufgabe(id);
  }
}

enum State {
  Default,
  Add,
  Edit
}

import { TestBed } from '@angular/core/testing';

import { AufgabenWebService } from './aufgaben-web.service';

describe('AufgabenWebService', () => {
  let service: AufgabenWebService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AufgabenWebService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

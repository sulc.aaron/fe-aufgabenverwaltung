import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Endpoint, SuccessResponse } from '../models/SuccessResponse';
import { ErrorResponse } from '../models/ErrorResponse';
import { Aufgabe } from '../models/Aufgabe';

@Injectable({
  providedIn: 'root'
})
export class AufgabenWebService {
  successEmitter: EventEmitter<SuccessResponse>;
  errorEmitter: EventEmitter<ErrorResponse>;
  apiUrl = 'http://127.0.0.1:8000/api';
  options: any;

  constructor(
    private http: HttpClient
  ) {
    this.options = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }),
      observe: 'response'
    };

    this.successEmitter = new EventEmitter();
    this.errorEmitter = new EventEmitter();
  }

  public getAlleAufgaben(): void {
    this.http.get(this.apiUrl + '/aufgaben', this.options).subscribe(
      (response: any) => {
        const aufgabenListe: Aufgabe[] = [];
        response.body.forEach((aufgabe: { id: number; titel: string; beschreibung: string; }) => {
          aufgabenListe.push(new Aufgabe(aufgabe.id, aufgabe.titel, aufgabe.beschreibung));
        });

        this.successEmitter.emit(new SuccessResponse(
          Endpoint.GetAlleAufgaben,
          response.status,
          aufgabenListe
        ));
      },
      (error: any) => {
        this.errorEmitter.emit(new ErrorResponse(
          Endpoint.GetAlleAufgaben,
          error.status,
          error.error.message
        ))
      }
    );
  }

  public addAufgabe(titel: string, beschreibung: string) {
    const postBody = {
      titel: titel,
      beschreibung: beschreibung
    }

    this.http.post(this.apiUrl + '/aufgabe/erstellen', postBody, this.options).subscribe(
      (response: any) => {
        this.successEmitter.emit(new SuccessResponse(
          Endpoint.PostAufgabeErstellen,
          response.status
        ));
      },
      (error: any) => {
        let errorMessage = error.error.message;
        if (undefined === errorMessage) {
          error.error.forEach((error: any) => {
            if (undefined === errorMessage) {
              errorMessage = error;
            }
            else {
              errorMessage = errorMessage + ' ' + error;
            }
          });
        }
        this.errorEmitter.emit(new ErrorResponse(
          Endpoint.PostAufgabeAnpassen,
          error.status,
          errorMessage
        ))
      }
    );
  }

  public editAufgabe(id: number, titel: string, beschreibung: string) {
    const postBody = {
      id: id,
      titel: titel,
      beschreibung: beschreibung
    }

    this.http.post(this.apiUrl + '/aufgabe/anpassen', postBody, this.options).subscribe(
      (response: any) => {
        this.successEmitter.emit(new SuccessResponse(
          Endpoint.PostAufgabeAnpassen,
          response.status
        ));
      },
      (error: any) => {
        let errorMessage = error.error.message;
        if (undefined === errorMessage) {
          error.error.forEach((error: any) => {
            if (undefined === errorMessage) {
              errorMessage = error;
            }
            else {
              errorMessage = errorMessage + ' ' + error;
            }
          });
        }
        this.errorEmitter.emit(new ErrorResponse(
          Endpoint.PostAufgabeAnpassen,
          error.status,
          errorMessage
        ))
      }
    );
  }

  public deleteAufgabe(id: number) {
    const postBody = {
      id: id,
    }

    this.http.post(this.apiUrl + '/aufgabe/entfernen', postBody, this.options).subscribe(
      (response: any) => {
        this.successEmitter.emit(new SuccessResponse(
          Endpoint.PostAufgabeEntfernen,
          response.status
        ));
      },
      (error: any) => {
        let errorMessage = error.error.message;
        if (undefined === errorMessage) {
          error.error.forEach((error: any) => {
            if (undefined === errorMessage) {
              errorMessage = error;
            }
            else {
              errorMessage = errorMessage + ' ' + error;
            }
          });
        }
        this.errorEmitter.emit(new ErrorResponse(
          Endpoint.PostAufgabeEntfernen,
          error.status,
          errorMessage
        ))
      }
    );
  }
}
